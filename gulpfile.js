var gulp        = require('gulp');
var runsequence = require('run-sequence');
var stylus      = require('gulp-stylus');
var plumber     = require('gulp-plumber');
var webserver   = require('gulp-webserver');
var fs          = require('fs');
var handlebars  = require('gulp-compile-handlebars');
var rename      = require('gulp-rename');
var download    = require('gulp-download');
var zip         = require('gulp-zip');

gulp.task('default', ['stylus', 'webserver'], function() {
	runsequence('download', 'template');

	gulp.watch(['css/stylus/**/*.styl'], ['stylus']);
	gulp.watch('templates/**/*.hbs', ['template']);
});

gulp.task('download', function() {
	return download('http://api.bowtie.dev/v1/themes/sampledata')
		.pipe(rename('data.json'))
		.pipe(gulp.dest('.'));
});

gulp.task('webserver', function() {
	return gulp.src('.')
		.pipe(webserver({
			livereload: true,
			open: false,
			port: 5000
		}));
});

gulp.task('stylus', function() {
	return gulp.src([
		'css/stylus/global.styl',
		'css/stylus/print.styl'
	])
		.pipe(plumber())
		.pipe(stylus())
		.pipe(gulp.dest('css'));
});

gulp.task('template', function() {
	var data = JSON.parse(fs.readFileSync('data.json', 'utf8'));
	var partials = 'templates/partials';

	return gulp.src('templates/index.hbs')
		.pipe(handlebars(data, {
			batch: partials,
			helpers: {
				includeBlueprint: function(section, options) {
					var blueprint = data.blueprints.filter(function(blueprint) {
						return blueprint.id == section.blueprint;
					})[0];

					var filecontent = fs.readFileSync('templates/partials/' + blueprint.name.toLowerCase() + '.hbs', 'utf8');
					var template = handlebars.Handlebars.compile(filecontent);

					return new handlebars.Handlebars.SafeString(template(section));
				}
			}
		}))
		.pipe(rename('index.html'))
		.pipe(gulp.dest('./'));
});

gulp.task('zip', function() {
	var files = [
		'./**/*',
		'!./node_modules/**',
		'!./.git/**'
	];
	gulp.src(files)
		.pipe(zip('theme.zip'))
		.pipe(gulp.dest('.'));
});
